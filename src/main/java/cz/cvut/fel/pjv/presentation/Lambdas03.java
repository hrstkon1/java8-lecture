package cz.cvut.fel.pjv.presentation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.List;

/**
 * @author Ondrej Hrstka
 */
public class Lambdas03 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> words = Lambdas01.loadLipsum();


        boolean ascending = false;
        Comparator<String> comparator = (o1, o2) -> {
            int order = ascending ? 1 : -1;
            return order * (o1.length() - o2.length());
        };


        words.sort(comparator);
        System.out.println(words);
    }

}
