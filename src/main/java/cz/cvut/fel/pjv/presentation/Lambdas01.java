package cz.cvut.fel.pjv.presentation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Ondrej Hrstka
 */
public class Lambdas01 {


    public static List<String> loadLipsum() throws IOException, URISyntaxException {
        Path path = Paths.get(Lambdas01.class.getResource("/lipsum.txt").toURI());
        //Won't work in jar :-(
        List<String> lines = Files.readAllLines(path);

        List<String> data = new ArrayList<>();

        for (String line : lines) {
            String[] words = line.replace(",", "")
                    .replace(".", "")
                    .split("\\s");
            for (String word : words) {
                if (!word.isEmpty()) {
                    data.add(word);
                }
            }
        }

        return data;
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> words = loadLipsum();

//        Comparator<String> comparator = new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.length() - o2.length();
//            }
//        };

        Comparator<String> comparator = (o1, o2) -> o1.length() - o2.length();
        Collections.sort(words, comparator);
        System.out.println(words);
    }



}
