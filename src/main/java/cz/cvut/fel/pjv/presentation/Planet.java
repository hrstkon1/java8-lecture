package cz.cvut.fel.pjv.presentation;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Ondrej Hrstka
 */
public class Planet {

    private final String name;
    private final double radius;
    private final Set<Moon> moons;

    public Planet(String name, double radius) {
        this.name = name;
        this.radius = radius;
        this.moons = new HashSet<>();
    }

    public void addMoon(Moon moon) {
        //TODO HERE is a problem
        moon.setPlanet(this);
        moons.add(moon);
    }

    public String getName() {
        return name;
    }

    public double getRadius() {
        return radius;
    }

    public Set<Moon> getMoons() {
        return moons;
    }

    public boolean hasMoons() {
        return !moons.isEmpty();
    }
}
