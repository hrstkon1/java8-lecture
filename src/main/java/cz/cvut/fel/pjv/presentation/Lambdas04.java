package cz.cvut.fel.pjv.presentation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author Ondrej Hrstka
 */
public class Lambdas04 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> data = Lambdas01.loadLipsum();

//        Function<String, String> toUpperCaseFunction = new Function<String, String>() {
//            @Override
//            public String apply(String s) {
//                return s.toUpperCase();
//            }
//        };

//        Function<String, String> toUpperCaseFunction = s -> s.toUpperCase();

        Function<String, String> toUpperCaseFunction = String::toUpperCase;

        List<String> modified = applyOnEach(data, toUpperCaseFunction);

        System.out.println(data);
        System.out.println(modified);
    }

    public static <T, R> List<R> applyOnEach(List<T> input, Function<T, R> function) {
        List<R> output = new ArrayList<>(input.size());

        for (T t : input) {
            R r = function.apply(t);
            output.add(r);
        }

        return output;
    }

}
