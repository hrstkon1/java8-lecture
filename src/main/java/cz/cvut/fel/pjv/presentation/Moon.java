package cz.cvut.fel.pjv.presentation;

/**
 * @author Ondrej Hrstka
 */
public class Moon {

    private final String name;
    private Planet planet;
    private boolean visitedByHumans;

    public Moon(String name, boolean visitedByHumans) {
        this.name = name;
        this.planet = null;
        this.visitedByHumans = visitedByHumans;
    }

    public String getName() {
        return name;
    }

    public Planet getPlanet() {
        return planet;
    }

    public void setPlanet(Planet planet) {
        this.planet = planet;
    }

    public boolean isVisitedByHumans() {
        return visitedByHumans;
    }

    public void setVisitedByHumans(boolean visitedByHumans) {
        this.visitedByHumans = visitedByHumans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Moon moon = (Moon) o;

        if (visitedByHumans != moon.visitedByHumans) return false;
        if (name != null ? !name.equals(moon.name) : moon.name != null) return false;
        return planet != null ? planet.equals(moon.planet) : moon.planet == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (planet != null ? planet.hashCode() : 0);
        result = 31 * result + (visitedByHumans ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}
