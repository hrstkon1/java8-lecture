package cz.cvut.fel.pjv.presentation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Ondrej Hrstka
 */
public class Streams {

    public static void main(String[] args) {
        List<Planet> planets = initPlanets();

//        List<Planet> planetWithMoons = new ArrayList<>();
//        for (Planet planet : planets) {
//            if (planet.hasMoons()) {
//                planetWithMoons.add(planet);
//            }
//        }
//
//        Collections.sort(planetWithMoons, new Comparator<Planet>() {
//            @Override
//            public int compare(Planet o1, Planet o2) {
//                return Double.compare(o1.getRadius(), o2.getRadius());
//            }
//        });
//
//        List<String> planetsWithMoonsBySize = new ArrayList<>();
//        for (Planet planetWithMoon : planetWithMoons) {
//            planetsWithMoonsBySize.add(planetWithMoon.getName());
//        }


        List<String> planetsWithMoonsBySize = planets.stream()
                .filter(Planet::hasMoons)
                .sorted(Comparator.comparing(Planet::getRadius).reversed())
                .map(Planet::getName)
                .collect(Collectors.toList());

        System.out.println(planetsWithMoonsBySize);

        List<Set<Moon>> moonsMap = planets.stream()
                .map(planet -> planet.getMoons())
                .collect(Collectors.toList());

        List<Moon> moonsFlatMap = planets.stream()
                .flatMap(planet -> planet.getMoons().stream())
                .collect(Collectors.toList());

        System.out.println(moonsMap);
        System.out.println(moonsFlatMap);

        int totalNumberOfMoons = planets.stream()
                .map(planet -> planet.getMoons().size())
                .reduce((integer, integer2) -> integer + integer2)
                .orElseGet(() -> 0);

        System.out.println("totalNumberOfMoons = " + totalNumberOfMoons);
    }

    private static List<Planet> initPlanets() {
        Planet jupiter = new Planet("Jupiter", 66911);
        Planet saturn = new Planet("Saturn", 55232);
        Planet uranus = new Planet("Uranus", 25362);
        Planet neptun = new Planet("Neptun", 24622);
        Planet earth = new Planet("Earth", 6371);
        Planet venus = new Planet("Venus", 6051);
        Planet mars = new Planet("Mars", 3389);
        Planet mercury = new Planet("Mercury", 2439);
        Planet pluto = new Planet("Pluto", 1186);

        Moon ganymede = new Moon("Ganymede", false);
        Moon titan = new Moon("Titan", true);
        Moon callisto = new Moon("Callisto", false);
        Moon io = new Moon("Io", false);
        Moon moon = new Moon("Moon", true);
        Moon europa = new Moon("Europa", false);
        Moon triton = new Moon("Triton", false);
        Moon titania = new Moon("Titania", false);
        Moon rhea = new Moon("Rhea", false);
        Moon oberon = new Moon("Oberon", false);
        Moon iapetus = new Moon("Iapetus", false);
        Moon phobos = new Moon("Phobos", true);
        Moon charon = new Moon("Charon", false);
        Moon umbriel = new Moon("Umbriel", false);
        Moon ariel = new Moon("Ariel", false);
        Moon dione = new Moon("Dione", false);
        Moon tethys = new Moon("Tethys", false);
        Moon deimos = new Moon("Deimos", false);

        jupiter.addMoon(io);
        jupiter.addMoon(europa);
        jupiter.addMoon(ganymede);
        jupiter.addMoon(callisto);

        saturn.addMoon(tethys);
        saturn.addMoon(dione);
        saturn.addMoon(rhea);
        saturn.addMoon(titan);
        saturn.addMoon(iapetus);

        mars.addMoon(phobos);
        mars.addMoon(deimos);

        pluto.addMoon(charon);

        earth.addMoon(moon);

        neptun.addMoon(triton);

        uranus.addMoon(titania);
        uranus.addMoon(umbriel);
        uranus.addMoon(ariel);
        uranus.addMoon(oberon);

        return Arrays.asList(jupiter, saturn, uranus, neptun, earth, venus, mars, mercury, pluto);
    }
}
