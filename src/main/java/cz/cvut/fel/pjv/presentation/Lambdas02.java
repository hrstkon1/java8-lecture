package cz.cvut.fel.pjv.presentation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;

/**
 * @author Ondrej Hrstka
 */
public class Lambdas02 {
    public static <T, U, R> List<R> combine(List<T> list1, List<U> list2, BiFunction<T, U, R> combiner) {
        if (list1.size() != list2.size()) {
            throw new IllegalArgumentException("Size of lists was different." +
                    " Size of list 1 was "+list1.size() +", size of list2 was: "+list2.size());
        }

        List<R> output = new ArrayList<>(list1.size());
        Iterator<T> it1 = list1.iterator();
        Iterator<U> it2 = list2.iterator();
        while (it1.hasNext()) {
            T item1 = it1.next();
            U item2 = it2.next();

            R result = combiner.apply(item1, item2);
            output.add(result);
        }

        return output;
    }

    public static void main(String[] args) {
        List<String> words = Arrays.asList("Word1", "Word2", "Word3");
        List<Integer> multipliers = Arrays.asList(1, 2, 5);

//        BiFunction<String, Integer, String> combiner = new BiFunction<String, Integer, String>() {
//            @Override
//            public String apply(String string, Integer integer) {
//                StringBuilder sb = new StringBuilder();
//                for (int i = 0; i < integer; i++) {
//                    sb.append(string);
//                }
//                return sb.toString();
//            }
//        };

        BiFunction<String, Integer, String> combiner = (string, integer) -> {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < integer; i++) {
                sb.append(string);
            }
            return sb.toString();
        };

        List<String> result = combine(words, multipliers, combiner);

        System.out.println(words);
        System.out.println(multipliers);
        System.out.println(result);
    }
}
